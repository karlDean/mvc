<?php

class App {

    private $URL;
    private $CONTROLLER;

    function __construct(){

        $this->parseURL();
        $this->loadController();
        $this->callMethod();
    }

    private function parseURL(){

        $_url = isset($_GET['url']) ? $_GET['url'] : '';
        $_url = $_url ? explode('/', $_url) : [];
        $this->URL = $_url;
    }

    private function loadController(){

        if(isset($this->URL[0])){
            $_controller = $this->URL[0];
            array_shift($this->URL);
        } else {
            $_controller = 'home';
        }
        if(file_exists('app/controllers/'.$_controller.'.php')){
            require_once 'app/controllers/'.$_controller.'.php';
            $this->CONTROLLER = new $_controller;
        } else {
            $this->error();
        }
    }

    private function callMethod(){

        if(isset($this->URL[0])){
            $_method = $this->URL[0];
            array_shift($this->URL);
        } else {
            $_method = 'index';
        }
        if(method_exists($this->CONTROLLER, $_method)){
            $this->CONTROLLER->$_method(...$this->URL);
        } else {
            $this->error();
        }
    }
    
    private function error(){
        exit('404');
    }
}