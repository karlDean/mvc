<?php

class Controller {

    /**
     * Displays the view in front of the end user.
     *
     * @param  string $view Path of the view.
     * @param  array $data Data to be passed in view.
     * @return void
     */
    protected function view($view, $data = []){

        require_once 'app/views/'.$view.'.php';
    }
    
    /**
     * Loads the required model for manipulating data from the database.
     *
     * @param  string $model Name of Model.
     * @return void
     */
    protected function model($model){

        $model = $model.'_Model';
        require_once 'app/models/'.$model.'.php';
        return new $model();
    }
    
    /**
     * Loads the required library for a certain task.
     *
     * @param  string $lib Name of Library.
     * @return void
     */
    protected function libs($lib){

        require_once 'app/libs/'.$lib.'.php';
        return new $lib();
    }

}