<?php

class Database extends PDO {

    function __construct(){

        parent::__construct('mysql:host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
    }
    
    /**
     * Selects rows based on the query.
     *
     * @param  string $query MySQL SELECT query string.
     * @param  array $data Data to be passed to execute query with matching array name.
     * @return array
     */
    protected function SELECT($query, $data = []){

        $sth = $this->prepare($query);
        
        $qryparams = [];

        foreach ($data as $key => $value) {
            $qryparams[':' . $key] = $value;
        }

        $sth->execute($qryparams);
        $result = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }
    
    /**
     * Inserts new row.
     *
     * @param  string $table Name of the table where new row will be inserted.
     * @param  array $data Data to be passed to execute insert query. Array names must match column names of the table.
     * @return integer
     */
    protected function INSERT($table, $data){

        $columns = [];
        $values = [];
        $qryparams = [];

        foreach ($data as $key => $value) {
            array_push($columns, $key);
            array_push($values, ':' . $key);
            $qryparams[':' . $key] = $value;
        }

        $qrystr = "INSERT INTO {$table} (" . implode(', ', $columns) . ") VALUES (" . implode(', ', $values) . ");";
        $sth = $this->prepare($qrystr);
        $sth->execute($qryparams);
        $id = $this->lastInsertId();

        return $id;
    }
    
    /**
     * Updates a row based on the given condition.
     *
     * @param  string $table Name of the table.
     * @param  array $data Data to be passed to execute update query. Array names must match column names of the table. Keys in condition must be included.
     * @param  string $cond Condition for selecting row and updating it. eg. "id = :id and name = :name"
     * @return boolean
     */
    protected function UPDATE($table, $data, $cond){
        $updates = [];
        $qryparams = [];

        foreach ($data as $key => $value) {
            array_push($updates, $key . ' = :' . $key);
            $qryparams[':' . $key] = $value;
        }

        $qrystr = "UPDATE {$table} SET " . implode(', ', $updates) . " WHERE {$cond}";
        $sth = $this->prepare($qrystr);
        $result = $sth->execute($qryparams);
        
        return $result;
    }
    
    /**
     * Deletes a row based on the given condition.
     *
     * @param  string $table Name of the table.
     * @param  array $data Data to be passed to execute delete query. Keys in condition must be included.
     * @param  string $cond Condition for selecting row and deleting it. eg. "id = :id and name = :name"
     * @return boolean
     */
    function DELETE($table, $data, $cond){

        $qryparams = [];

        foreach ($data as $key => $value) {
            $qryparams[':' . $key] = $value;
        }

        $qrystr = "DELETE FROM {$table} WHERE {$cond}";

        $sth = $this->prepare($qrystr);
        $result = $sth->execute($qryparams);
        
        return $result;
    }
    
}
