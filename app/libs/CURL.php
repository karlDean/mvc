<?php

class CURL {
	
	public function curl($url,$post,$header_post=[],$customrequest = 'POST') 
	{
		$value='';
			
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_CUSTOMREQUEST => $customrequest,
		  CURLOPT_POSTFIELDS => $post,
		  CURLOPT_HTTPHEADER => $header_post,
		  CURLOPT_SSL_VERIFYPEER=>0
		));

		if (isset($_SERVER['HTTP_USER_AGENT'])) {
			curl_setopt_array($curl, array(CURLOPT_USERAGENT=>$_SERVER['HTTP_USER_AGENT']));
		}

		$value = curl_exec($curl);
		$value.=curl_error($curl);
        curl_close($curl);
        
		return $value;
	}
}