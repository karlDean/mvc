<?php

class Responses {
    
    public static function output($code, $data = []){

        $rescode;
        $msg;

        switch ($code) {
            case '1':
                $rescode = 1;
                $msg = 'OK';
                break;
            
            default:
                # code...
                break;
        }

        return [
            "responsecode" => $rescode,
            "responsemsg" => $msg,
            "responsedetails" => $data
        ];
    }

    public static function exit_now($code, $data = []){

        $response = self::output($code);
        self::send($response);
    }

    public static function send($response){
        header('Content-Type: application/json');
        exit(json_encode($response));
    }
}