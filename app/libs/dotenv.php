<?php

$dotenv = fopen('app/.env', 'r') or die('File error: .env');

while(!feof($dotenv)){
    $envVar = fgets($dotenv);
    if(strlen(trim($envVar))){
        $envVar = explode('=', $envVar);
        if(!$envVar[0])
            throw new Exception('Environment Variable Name cannot be empty string.');
        define(trim($envVar[0]), trim($envVar[1]));
    }
}
