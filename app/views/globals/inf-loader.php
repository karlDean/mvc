<link rel="stylesheet" href="<?=CSS_PATH?>globals.inf-loader.css">

<div class="inf-loader">
    <div class="inf-loader-inner">
        <svg viewBox="0 0 50 50">
            <!-- <?php for ($i = 1; $i < 5; $i++): ?>
            <path d="M 0 <?= $i * 10 ?> l 100 0" stroke="white" stroke-width=".1"/>
            <?php endfor ?>

            <?php for ($i = 1; $i < 5; $i++): ?>
            <path d="M <?= $i * 10 ?> 0 l 0 100" stroke="white" stroke-width=".1"/>
            <?php endfor ?> -->

            <path id="infinity" d="M 25 25 l -5 -5 q -5 -4.2 -10 0 -4.2 5 0 10 5 4.2 10 0 l 10 -10 q 5 -4.2 10 0 4.2 5 0 10 -5 4.2 -10 0 Z" fill="none" stroke="url(#linearColorss)" stroke-linecap="round" stroke-width=".5" />
            
            <linearGradient id="linearColorss" x1="0" y1="0" x2="1" y2="0">
                <stop offset="0%" stop-color="#aef8e800"></stop>
                <stop offset="20%" stop-color="#aef8e811"></stop>
                <stop offset="50%" stop-color="#e0fff8"></stop>
                <stop offset="80%" stop-color="#aef8e811"></stop>
                <stop offset="100%" stop-color="#aef8e800"></stop>
            </linearGradient>
        </svg>
    </div>
</div>