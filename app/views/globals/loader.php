<div class="loader">

    <link rel="stylesheet" href="<?=CSS_PATH?>globals.loader.css">

    <div class="glow">
        <span></span>
    </div>
    <div class="rotater">
        <span class="dot"></span>
        <svg viewBox="0 0 100 100">
            <path id="Path_1" data-name="Path 1" d="M0-19.855a50.877,50.877,0,0,1,23.993-6.151,50.9,50.9,0,0,1,24,6.151" transform="translate(0.258 26.506)" fill="none" stroke="url(#linearColors)" stroke-width="1.5"/>
            <linearGradient id="linearColors" x1="0" y1="0" x2="1" y2="0">
                <stop offset="0%" stop-color="#aef8e800"></stop>
                <stop offset="20%" stop-color="#aef8e811"></stop>
                <stop offset="50%" stop-color="#aef8e8"></stop>
                <stop offset="80%" stop-color="#aef8e811"></stop>
                <stop offset="100%" stop-color="#0e201c00"></stop>
            </linearGradient>
        </svg>
    </div>
    <div class="main-circle">
        <svg viewBox="0 0 100 100">
            <g id="Ellipse_1" data-name="Ellipse 1" fill="#fff" stroke="#fafafa" stroke-width="1">
                <circle cx="50" cy="50" r="49" fill="none"/>
            </g>
        </svg>
    </div>
</div>