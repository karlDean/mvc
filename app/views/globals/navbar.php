<div class="navbar">

    <link rel="stylesheet" href="<?=CSS_PATH?>globals.navbar.css">

    <div class="logo">
        <p>NEON</p>
    </div>
    <div class="navs">
        <ul>
            <li>
                <svg viewBox="0 0 140 80">
                    <rect width="105" height="45" fill="none" stroke="#e1fff7" stroke-width="2"/>
                </svg>
                Home
            </li>
            <li>
                <svg viewBox="0 0 140 80">
                    <rect width="105" height="45" fill="none" stroke="#e1fff7" stroke-width="2"/>
                </svg>
                About us
            </li>
            <li>
                <svg viewBox="0 0 140 80">
                    <rect width="105" height="45" fill="none" stroke="#e1fff7" stroke-width="2"/>
                </svg>
                Contact us
            </li>
        </ul>
    </div>
</div>