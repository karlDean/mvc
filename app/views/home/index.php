<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="<?=CSS_PATH?>home.index.css">
    <link rel="stylesheet" href="<?=ASSETS_PATH?>css/aos.css">
</head>
<body>
    <div class="main">

        <?php 
            include 'app/views/globals/loader.php'; 
            include 'app/views/globals/navbar.php';
            include 'app/views/globals/inf-loader.php';
        ?>

        <div class="bg">
            <img src="<?=IMG_PATH?>steel.jpg" alt="">
        </div>

        
    </div>

    <script src="<?=ASSETS_PATH?>js/jquery.dev.js"></script>
    <script src="<?=ASSETS_PATH?>js/aos.js"></script>
    <script src="<?=ASSETS_PATH?>js/chart.min.js"></script>
    <script src="<?=JS_PATH?>home/index.js"></script>
</body>
</html>

<script>
    // AOS.init();
</script>