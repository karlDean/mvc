<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=CSS_PATH?>mySlide.index.css">
    <link rel="stylesheet" href="<?=CSS_PATH?>mySlide.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>MySlide</title>
</head>
<body>

    <div class="main-container">
        <!-- <h1>Sample Slides</h1>
        <div class="sample-slides">

            <my-slides id="my-sample-slide">
                <my-slide class='slide'>
                    <h1>slide 1</h1>
                </my-slide>
                <my-slide class='slide'>
                    <h1>slide 2</h1>
                </my-slide>
                <my-slide class='slide'>
                    <h1>slide 3</h1>
                    <br>
                    <p>asdasdsadsd</p>
                </my-slide>
                <my-slide class='slide'>
                    <h1>slide 4</h1>
                </my-slide>
                <my-slide class='slide'>
                    <h1>slide 5</h1>
                </my-slide>
            </my-slides>

        </div>
        <button id='next-btn'>Next</button>
        <button id='prev-btn'>Previous</button>
        <input type="number" name="slideindex" id="slideindex">
        <button id='slide-to-btn'>Slide to</button>
        <button id='active-slide-btn'>Active Slide</button>
        <button id='autoplay-btn'>Autoplay</button>
        <button id='rev-autoplay-btn'>Reversed Autoplay</button>
        <button id='stop-autoplay-btn'>Stop Autoplay</button> -->

        <div class="modal">

            <div class="modal-inner">

                <div class="modal-inner-header">
                    <i class="fa fa-close"></i>
                </div>

                <div class="modal-inner-content">

                    <my-slides id="modal-slide">

                        <my-slide class='modal-slide-item'>
                            <h1>How may I help you today?</h1>
                            <select name="helpOpt" id="helpOpt">
                                <option value="1">I have a question about my purchased.</option>
                            </select>
                        </my-slide>

                        <my-slide class='modal-slide-item'>
                            <div class="descriptor-search">
                                <label>Enter Descriptor:</label>
                                <input type="text" name="descriptor_key" id="descriptor-key">
                            </div>
                            <i>*All fields below are required</i>
                        </my-slide>

                    </my-slides>

                </div>

                <div class="modal-inner-footer">
                    <button id="modal-back">Back</button>
                    <button id="modal-next" class='primary'>Next</button>
                </div>

            </div>

        </div>

    </div>
    

    <script src="<?=ASSETS_PATH?>js/mySlide.js"></script>
    <script src="<?=ASSETS_PATH?>js/jquery.dev.js"></script>
    <script src="<?=JS_PATH?>mySlide/index.js"></script>
</body>
</html>