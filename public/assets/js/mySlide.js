class MySlides extends HTMLElement {

    slides = [];
    cur_slide = 0;
    slide1;
    slide2;
    slides_container;
    ontransition = false;
    autoSlideInterval;

    constructor() {
        super();

        let slides = this.querySelectorAll('my-slide');
        
        for (let slide of slides) {
            this.slides.push({
                class: slide.className,
                id: slide.id,
                content: slide.innerHTML.trim()
            });
            slide.remove();
        }

        let viewport = document.createElement('div');
        viewport.className = 'my-slides-viewport';
        this.appendChild(viewport);

        this.slides_container = document.createElement('div');
        this.slides_container.className = 'my-slides-container';
        viewport.appendChild(this.slides_container);

        this.slide1 = document.createElement('div');
        this.slide1.className = 'my-slides-slide my-slides-slide1';
        this.slide2 = document.createElement('div');
        this.slide2.className = 'my-slides-slide my-slides-slide2';
        this.slides_container.append(this.slide1, this.slide2);

        let setActive = parseInt(this.getAttribute('setActive')) - 1;
        this.cur_slide = setActive ? setActive : 0;

        this.slide1.innerHTML = this.slides[setActive ? setActive : 0].content;
        this.slide1.className += ' ' + this.slides[setActive ? setActive : 0].class;
        this.slide1.id += ' ' + this.slides[setActive ? setActive : 0].id;
    }

    slideTo(index) {

        if (index - 1 < this.cur_slide)
            this.prevSlide(index - 1);
        else if(index - 1 > this.cur_slide)
            this.nextSlide(index - 1);
    }

    nextSlide(index = null) {

        let curslide = this.slides[this.cur_slide];
        let nextslide = this.slides[index != null ? index : this.cur_slide + 1];

        if (!nextslide || this.ontransition)
            return;
        
        this.ontransition = true;
        
        this.className = this.className.replace(/slide-to-left|slide-to-right|my-slides-on-end/g, '').trim();
        
        this.slide2.innerHTML = nextslide.content;
        this.slide2.className = 'my-slides-slide my-slides-slide2 ' + nextslide.class;
        this.slide2.id = nextslide.id;

        this.slide1.innerHTML = curslide.content;
        this.slide1.className = 'my-slides-slide my-slides-slide1 ' + curslide.class;
        this.slide1.id = curslide.id;
        
        setTimeout(() => {
            this.slide1.innerHTML = '';
            this.slide1.className = 'my-slides-slide my-slides-slide1';
            this.slide1.id = '';
        }, 400);

        setTimeout(() => {
            this.className += ' slide-to-left';    
        }, 10);

        setTimeout(() => {
            this.ontransition = false;
        }, 600);

        this.cur_slide = index != null ? index : this.cur_slide + 1;
    }

    prevSlide(index = null) {
        let curslide = this.slides[this.cur_slide];
        let prevslide = this.slides[index != null ? index : this.cur_slide - 1];

        if (!prevslide || this.ontransition)
            return;
        
        this.ontransition = true;
        
        this.className = this.className.replace(/slide-to-left|slide-to-right|my-slides-on-end/g, '').trim();
        this.className += ' my-slides-on-end';

        this.slide2.innerHTML = curslide.content;
        this.slide2.className = 'my-slides-slide my-slides-slide2 ' + curslide.class;
        this.slide2.id = curslide.id;

        this.slide1.innerHTML = prevslide.content;
        this.slide1.className = 'my-slides-slide my-slides-slide2 ' + prevslide.class;
        this.slide1.id = prevslide.id;

        setTimeout(() => {
            this.slide2.innerHTML = '';
            this.slide2.className = 'my-slides-slide my-slides-slide2';
            this.slide2.id = '';
        }, 400);

        setTimeout(() => {
            this.className = ' slide-to-right';
        }, 10);

        setTimeout(() => {
            this.ontransition = false;
        }, 600);

        this.cur_slide = index != null ? index : this.cur_slide - 1;
    }

    activeSlide() {
        return this.cur_slide + 1;
    }

    autoplay(duration, reversed = false) {
        if (this.autoSlideInterval)
            return;

        if (!reversed) {
            this.autoSlideInterval = setInterval(() => {
                if (this.slides.length == this.cur_slide + 1)
                    this.slideTo(1);
                else
                    this.nextSlide();
            }, duration);
        } else {
            this.autoSlideInterval = setInterval(() => {
            if (this.cur_slide == 0)
                this.slideTo(5);
            else
                this.prevSlide();
        }, duration);
        }
    }

    stopAutoplay() {
        clearInterval(this.autoSlideInterval);
    }
}

class MySlide extends HTMLElement {

    constructor() {
        super();

        this.style.display = 'none';
    }
} 

customElements.define('my-slides', MySlides);
customElements.define('my-slide', MySlide);


const SlideController = (selector) => {

    let slide = document.querySelector(selector);

    const func = {
        slideTo: (index) => {
            slide.slideTo(index);
            return func;
        },

        nextSlide: () => {
            slide.nextSlide();
            return func;
        },

        prevSlide: () => {
            slide.prevSlide();
            return func;
        },

        activeSlide: () => {
            return slide.activeSlide();
        },

        autoplay: (duration = 1000) => {
            slide.autoplay(duration);
            return func;
        },

        reversedAutoplay: (duration = 1000) => {
            slide.autoplay(duration, true);
        },

        stopAutoplay: () => {
            slide.stopAutoplay();
            return func;
        }
    }

    return func;

}
