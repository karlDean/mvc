const neon_btns = $('.neon-btn');

for (let btn of neon_btns) {
    console.log(btn);
    let text = $(btn).text();
    $(btn).text('');
    $(btn).append(`
        <p>${text}</p>
        <svg viweBox='0 0 140 40' width="140" height="40" stroke="#183c34" fill="#1c2220">
            <path d="M 10 15 v 18 l 10 10" fill="none" stroke="#183c34"/>
            <path d="M120 0 l 10 10 v 15" fill="none" stroke="#183c34"/>
            <path d="M 20 0 h 90 l 10 10 v 30 h -90 l -10 -10 v -30" stroke="#183c34"/>
        </svg>
    `);

}