$('#next-btn').click(function () {
    SlideController('#my-sample-slide').nextSlide();
})

$('#prev-btn').click(function () {
    SlideController('#my-sample-slide').prevSlide();
})

$('#slide-to-btn').click(function () {

    let index = $('#slideindex').val();

    SlideController('#my-sample-slide').slideTo(index);
})

$('#active-slide-btn').click(function () {
    let activeSlide = SlideController('#my-sample-slide').activeSlide();
    console.log(activeSlide);
})

$('#autoplay-btn').click(function () {
    SlideController('#my-sample-slide').autoplay();
})

$('#rev-autoplay-btn').click(function () {
    SlideController('#my-sample-slide').reversedAutoplay();
})

$('#stop-autoplay-btn').click(function () {
    SlideController('#my-sample-slide').stopAutoplay();
})

$('#modal-next').click(function () {
    SlideController('#modal-slide').nextSlide();
})

$('#modal-back').click(function () {
    SlideController('#modal-slide').prevSlide();
})